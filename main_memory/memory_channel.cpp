/*
 * Copyright (C) 2009~2015  Marco Antonio Zanata Alves
 *                          (mazalves at inf.ufrgs.br)
 *                          GPPD - Parallel and Distributed Processing Group
 *                          Universidade Federal do Rio Grande do Sul
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../sinuca.hpp"

#ifdef MEMORY_CONTROLLER_DEBUG
    #define MEMORY_CONTROLLER_DEBUG_PRINTF(...) DEBUG_PRINTF(__VA_ARGS__);
#else
    #define MEMORY_CONTROLLER_DEBUG_PRINTF(...)
#endif

// ============================================================================
memory_channel_t::memory_channel_t() {
    this->bank_per_channel = 0;
    this->bank_buffer_size = 0;
    this->bank_selection_policy = SELECTION_ROUND_ROBIN;
    this->request_priority_policy = REQUEST_PRIORITY_ROW_BUFFER_HITS_FIRST;
    this->write_priority_policy = WRITE_PRIORITY_DRAIN_WHEN_FULL;

    this->packages_inside_channel = 0;

    this->not_column_bits_mask = 0;
    this->row_bits_mask = 0;

    this->bank_bits_mask = 0;
    this->bank_bits_shift = 0;

    this->bank_buffer = NULL;
    this->bank_buffer_actual_position = NULL;

    this->bank_is_drain_write = NULL;
    this->bank_number_drain_write = NULL;
    this->bank_open_row_address = NULL;
    this->bank_rowcol_state = NULL;

    this->bank_last_command = NULL;
    this->bank_last_command_cycle = NULL;
    this->channel_last_command_cycle = NULL;

    this->last_bank_selected = 0;
};

// ============================================================================
memory_channel_t::~memory_channel_t() {
    utils_t::template_delete_array<container_ptr_memory_package_t>(bank_buffer);
    utils_t::template_delete_array<int32_t>(bank_buffer_actual_position);

    utils_t::template_delete_array<bool>(bank_is_ready_to_transmit);

    utils_t::template_delete_array<bool>(bank_is_drain_write);
    utils_t::template_delete_array<uint32_t>(bank_number_drain_write);

    utils_t::template_delete_array<uint64_t>(bank_open_row_address);
    utils_t::template_delete_array<bool>(bank_rowcol_state);

    utils_t::template_delete_array<memory_controller_command_t>(bank_last_command);
    utils_t::template_delete_matrix<uint64_t>(bank_last_command_cycle, this->get_bank_per_channel());
    utils_t::template_delete_array<uint64_t>(channel_last_command_cycle);
};

// ============================================================================
void memory_channel_t::allocate() {

    this->bank_buffer = utils_t::template_allocate_array<container_ptr_memory_package_t>(this->get_bank_per_channel());
    this->bank_buffer_actual_position = utils_t::template_allocate_initialize_array<int32_t>(this->get_bank_per_channel(), -1);

    this->bank_is_ready_to_transmit = utils_t::template_allocate_initialize_array<bool>(this->get_bank_per_channel(), false);

    this->bank_is_drain_write = utils_t::template_allocate_initialize_array<bool>(this->get_bank_per_channel(), false);
    this->bank_number_drain_write = utils_t::template_allocate_initialize_array<uint32_t>(this->get_bank_per_channel(), 0);
    this->bank_open_row_address = utils_t::template_allocate_initialize_array<uint64_t>(this->get_bank_per_channel(), 0);
    this->bank_rowcol_state = utils_t::template_allocate_initialize_array<bool>(this->get_bank_per_channel(), false);
    
    this->bank_last_command = utils_t::template_allocate_initialize_array<memory_controller_command_t>(this->get_bank_per_channel(), MEMORY_CONTROLLER_COMMAND_ROW_ACCESS);
    this->bank_last_command_cycle = utils_t::template_allocate_initialize_matrix<uint64_t>(this->get_bank_per_channel(), MEMORY_CONTROLLER_COMMAND_NUMBER, 0);
    this->channel_last_command_cycle = utils_t::template_allocate_initialize_array<uint64_t>(MEMORY_CONTROLLER_COMMAND_NUMBER, 0);
};

// ============================================================================
int32_t memory_channel_t::find_next_read_operation(uint32_t bank) {
    ERROR_ASSERT_PRINTF(this->bank_buffer[bank].size() > 0, "Calling find_next_operation with empty buffer\n")

    int32_t slot = POSITION_FAIL;
    uint32_t i;

    switch (this->request_priority_policy) {
        case REQUEST_PRIORITY_ROW_BUFFER_HITS_FIRST:
            /// Try to find OPERATION in the same OPEN_ROW
            for (i = 0; i < this->bank_buffer[bank].size(); i++) {
	    	if ((this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_SPECIAL_GATHER) &&
		   (this->cmp_col_bank_channel(this->bank_buffer[bank][i]->memory_address, this->bank_open_row_address[bank]) &&
		    (this->bank_rowcol_state[bank] == true))){
			//special gather matches COL
			//send this!
			slot = i;
			//printf("got a col hit\n");
			break;	
		}	

   	    	if (this->cmp_row_bank_channel(this->bank_buffer[bank][i]->memory_address, this->bank_open_row_address[bank]) &&
			(this->bank_rowcol_state[bank] == false)) 
		{
                    if (this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_READ ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_INST ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_PREFETCH ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_HMC_ROA ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_HMC_ROWA
		    ) {
                        slot = i;
                        break;
                    }
                }
            }
            /// If could not find, Try to find OLDER OPERATION
            if (slot == POSITION_FAIL) {
                for (i = 0; i < this->bank_buffer[bank].size(); i++) {
                    if (this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_READ ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_INST     ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_PREFETCH ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_HMC_ROA  ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_HMC_ROWA || 
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_SPECIAL_GATHER
		    ) {
                        slot = i;
                        break;
                    }
                }
            }
        break;

        case REQUEST_PRIORITY_FIRST_COME_FIRST_SERVE:
            /// Try to find OLDER OPERATION
            for (i = 0; i < this->bank_buffer[bank].size(); i++) {
                if (this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_READ ||
                this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_INST ||
                this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_PREFETCH ||
                this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_HMC_ROA ||
                this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_HMC_ROWA ||
                this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_SPECIAL_GATHER) {
                    slot = i;
                    break;
                }
            }
        break;
    }
    return slot;
};

// ============================================================================
int32_t memory_channel_t::find_next_write_operation(uint32_t bank) {
    ERROR_ASSERT_PRINTF(this->bank_buffer[bank].size() > 0, "Calling find_next_operation with empty buffer\n")

    int32_t slot = POSITION_FAIL;
    uint32_t i;

    switch (this->request_priority_policy) {
        case REQUEST_PRIORITY_ROW_BUFFER_HITS_FIRST:
            /// Try to find OPERATION in the same OPEN_ROW
            for (i = 0; i < this->bank_buffer[bank].size(); i++) {
                if ((this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_WRITEBACK ||
                this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_WRITE)
                &&
                this->cmp_row_bank_channel(this->bank_buffer[bank][i]->memory_address, this->bank_open_row_address[bank])
		&&
		this->bank_rowcol_state[bank] == false) {
                    slot = i;
                    break;
                }
            }
            /// If could not find, Try to find OLDER OPERATION
            if (slot == POSITION_FAIL) {
                for (i = 0; i < this->bank_buffer[bank].size(); i++) {
                    if (this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_WRITEBACK ||
                    this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_WRITE) {
                        slot = i;
                        break;
                    }
                }
            }
        break;

        case REQUEST_PRIORITY_FIRST_COME_FIRST_SERVE:
            /// Try to find OLDER OPERATION
            for (i = 0; i < this->bank_buffer[bank].size(); i++) {
                if (this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_WRITEBACK ||
                this->bank_buffer[bank][i]->memory_operation == MEMORY_OPERATION_WRITE) {
                    slot = i;
                    break;
                }
            }
        break;
    }
    return slot;
};

// ============================================================================
int32_t memory_channel_t::find_next_package(uint32_t bank) {

    if (this->bank_buffer[bank].size() == 0) {
        return POSITION_FAIL;
    }

    int32_t slot = POSITION_FAIL;

    /// If it is in drain_write mode
    if (this->bank_is_drain_write[bank] && this->bank_number_drain_write[bank] > 0) {
        slot = find_next_write_operation(bank);
        if (slot == POSITION_FAIL) {
            this->bank_is_drain_write[bank] = false;
            this->bank_number_drain_write[bank] = 0;
            slot = find_next_read_operation(bank);
        }
        else {
            this->bank_number_drain_write[bank]--;
        }
    } else {
        switch (this->write_priority_policy) {
            case WRITE_PRIORITY_DRAIN_WHEN_FULL:
                slot = find_next_read_operation(bank);
                /// Could not find READ, but buffer is FULL
                if (slot == POSITION_FAIL && this->bank_buffer[bank].size() == this->bank_buffer_size) {
                    this->bank_is_drain_write[bank] = true;
                    this->bank_number_drain_write[bank] = this->bank_buffer_size - 1;
                    slot = find_next_write_operation(bank);
                }
            break;

            case WRITE_PRIORITY_SERVICE_AT_NO_READ:
                slot = find_next_read_operation(bank);
                /// Could not find READ
                if (slot == POSITION_FAIL) {
                    /// If buffer is full
                    if (this->bank_buffer[bank].size() == this->bank_buffer_size) {
                        this->bank_is_drain_write[bank] = true;
                        this->bank_number_drain_write[bank] = this->bank_buffer_size - 1;
                    }
                    slot = find_next_write_operation(bank);
                }
            break;
        }
    }
    return slot;
};


// ============================================================================
uint64_t memory_channel_t::get_minimum_latency(uint32_t bank, memory_controller_command_t next_command) {
    uint64_t max_cycle = 0;
    uint64_t a = 0;
    uint64_t b = 0;
    uint64_t c = 0;
    uint64_t d = 0;

    switch (next_command){
        case MEMORY_CONTROLLER_COMMAND_PRECHARGE:
            a = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] + this->timing_ras;
            b = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLUMN_READ] + this->timing_al + this->timing_rtp - this->timing_ccd; // + this->timing_burst; // tBurst will be controlled separated
            c = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] + this->timing_al + this->timing_cwd + this->timing_wr; // + this->timing_burst; // tBurst will be controlled separated
        break;

        case MEMORY_CONTROLLER_COMMAND_ROW_ACCESS:
        {
            /// Obtain the 4th newer RAS+FAW command amoung the banks.
            uint64_t last_ras = 0;
            for (uint32_t i = 0; i < this->get_bank_per_channel(); i++) {
                last_ras = this->bank_last_command_cycle[i][MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] + this->timing_faw;
                if ((a < last_ras) && (d = c) && (c = b) && (b=a) && (a=last_ras)) continue;
                if ((b < last_ras) && (d = c) && (c = b) && (b=last_ras)) continue;
                if ((c < last_ras) && (d = c) && (c = last_ras)) continue;
                if ((d < last_ras) && (d = last_ras)) continue;
            }
            /// 4th RAS + FAW window
            d += this->timing_faw;

            a = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_PRECHARGE] + this->timing_rp;
            b = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] + this->timing_rc;
            c = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] + this->timing_rrd;
        }
        break;
	case MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS:
	{
            /// Obtain the 4th newer RAS+FAW command amoung the banks.
            uint64_t last_ras = 0;
            for (uint32_t i = 0; i < this->get_bank_per_channel(); i++) {
                last_ras = this->bank_last_command_cycle[i][MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] + this->timing_faw;
                if ((a < last_ras) && (d = c) && (c = b) && (b=a) && (a=last_ras)) continue;
                if ((b < last_ras) && (d = c) && (c = b) && (b=last_ras)) continue;
                if ((c < last_ras) && (d = c) && (c = last_ras)) continue;
                if ((d < last_ras) && (d = last_ras)) continue;
            }
            /// 4th RAS + FAW window
            d += this->timing_faw;

            a = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_PRECHARGE] + this->timing_rp;
            b = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] + this->timing_rc * this->colbuff_strobe_count; //4 row cycles? 8 ? depends on number of reads
            c = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] + this->timing_rrd;
        
	}
	break;
        case MEMORY_CONTROLLER_COMMAND_COLUMN_READ:
            a = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] + this->timing_rcd - this->timing_al;
            b = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_READ];// + this->timing_burst; // tBurst will be controlled separated
            c = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_READ] + this->timing_ccd;
            d = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] + this->timing_cwd + this->timing_wtr;// + this->timing_burst; // tBurst will be controlled separated
        break;
        case MEMORY_CONTROLLER_COMMAND_COLBUFF_READ:
            a = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] + this->timing_rcd - this->timing_al;
            b = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_READ];// + this->timing_burst; // tBurst will be controlled separated
            c = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_READ] + this->timing_ccd;
            d = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_WRITE] + this->timing_cwd + this->timing_wtr;// + this->timing_burst; // tBurst will be controlled separated
        break;

        case MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE:
            a = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] + this->timing_rcd - this->timing_al;
            b = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_READ] + this->timing_cas;// + this->timing_burst; // tBurst will be controlled separated
            c = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE];// + this->timing_burst; // tBurst will be controlled separated
            d = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] + this->timing_ccd;
        break;
        case MEMORY_CONTROLLER_COMMAND_COLBUFF_WRITE:
            a = this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] + this->timing_rcd - this->timing_al;
            b = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_READ] + this->timing_cas;// + this->timing_burst; // tBurst will be controlled separated
            c = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_WRITE];// + this->timing_burst; // tBurst will be controlled separated
            d = this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_WRITE] + this->timing_ccd;
        break;

        case MEMORY_CONTROLLER_COMMAND_NUMBER:
            ERROR_PRINTF("Should not receive COMMAND_NUMBER\n")
        break;
    }

    /// Obtain the maximum value to be respected
    max_cycle = a;
    (max_cycle < b) && (max_cycle = b);
    (max_cycle < c) && (max_cycle = c);
    (max_cycle < d) && (max_cycle = d);

    return max_cycle;
};

// ============================================================================
package_state_t memory_channel_t::treat_memory_request(memory_package_t *package) {
    uint32_t bank = get_bank(package->memory_address);

    /// Try to insert into bank buffer
    if (this->bank_buffer[bank].size() < this->bank_buffer_size) {
        this->bank_buffer[bank].push_back(package);
        this->packages_inside_channel++;
        return PACKAGE_STATE_WAIT;
    }
    return PACKAGE_STATE_UNTREATED;
};

// ============================================================================
void memory_channel_t::clock(uint32_t subcycle) {
    if (subcycle != 0) return;
    MEMORY_CONTROLLER_DEBUG_PRINTF("==================== ID(%u) ", this->get_id());
    MEMORY_CONTROLLER_DEBUG_PRINTF("====================\n");
    MEMORY_CONTROLLER_DEBUG_PRINTF("cycle() \n");

    /// Nothing to be done this cycle. -- Improve the performance
    if (this->packages_inside_channel == 0) return;

    uint32_t bank = 0;
    switch (this->get_bank_selection_policy()) {
        case SELECTION_ROUND_ROBIN:
            bank = this->selection_bank_round_robin();
        break;

        case SELECTION_RANDOM:
            bank = this->selection_bank_random();
        break;

        case SELECTION_BUFFER_LEVEL:
            ERROR_PRINTF("Selection Policy: SELECTION_BUFFER_LEVEL not implemented.\n");
        break;
    }

    /// Select package to be treated
    if (this->bank_buffer_actual_position[bank] == -1)
        this->bank_buffer_actual_position[bank] = find_next_package(bank);

    /// If there is some package to be treated and it is not waiting to be transmitted
    if (this->bank_buffer_actual_position[bank] != POSITION_FAIL &&
    !this->bank_is_ready_to_transmit[bank]) 
    {

        memory_package_t *package = this->bank_buffer[bank][this->bank_buffer_actual_position[bank]];
        MEMORY_CONTROLLER_DEBUG_PRINTF("Channel Treating %s\n", package->content_to_string().c_str());

        /// FSM to control the following signals: PRECHARGE, ROW_ACCESS, COLUMN_READ/COLUMN_WRITE
        /// Considering the last signal, perform the next
        /// The COLUMN_READ and COLUMN_WRITE does not consider the burst latency (which is treated further in the code).
	//
	//FBM: Extended FSM, added COLBUFF_ACCESS, COLBUFF_READ, COLBUFF_WRITE
	//     Consider bank_colrow_state to check whether we have an open row (false) 
	//	or an open column (true)
        switch(this->bank_last_command[bank]) 
	{
            // =====================================================================
            case MEMORY_CONTROLLER_COMMAND_PRECHARGE:
                ERROR_ASSERT_PRINTF(cmp_row_bank_channel(this->bank_open_row_address[bank], package->memory_address), "Sending ROW_ACCESS to wrong row.")

		if (package->memory_operation == MEMORY_OPERATION_SPECIAL_GATHER){
			/// Respect Min. Latency
			if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS) > sinuca_engine.get_global_cycle()) {
			    MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet. RP->RAS for COLUMN (special gather)\n");
			    MEMORY_CONTROLLER_DEBUG_PRINTF("cycle %" PRIu64 ".\n", get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS));
			    break;
			}
		} else {
			/// Respect Min. Latency
			if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_ROW_ACCESS) > sinuca_engine.get_global_cycle()) {
			    MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet. RP->RAS\n");
			    MEMORY_CONTROLLER_DEBUG_PRINTF("cycle %" PRIu64 ".\n", get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_ROW_ACCESS));
			    break;
			}
		}

		if (this->bank_rowcol_state[bank] == false){
		    this->add_stat_row_buffer_miss();
		} else {
		    this->add_stat_col_buffer_miss();
		}
                MEMORY_CONTROLLER_DEBUG_PRINTF("PRECHARGE -> ROW_ACCESS.\n");
		if (package->memory_operation == MEMORY_OPERATION_SPECIAL_GATHER){
			this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS;
			this->bank_rowcol_state[bank] = true;//switch to open column
			this->bank_open_row_address[bank] = package->memory_address;
			this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] = sinuca_engine.get_global_cycle();
			this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] = sinuca_engine.get_global_cycle();
			this->add_stat_col_activates();
		} else {
			this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_ROW_ACCESS;
			this->bank_rowcol_state[bank] = false;	//switch to open row
			this->bank_open_row_address[bank] = package->memory_address;
			this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] = sinuca_engine.get_global_cycle();
			this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_ROW_ACCESS] = sinuca_engine.get_global_cycle();
			this->add_stat_row_activates();
		}
            break;

            // =====================================================================
            case MEMORY_CONTROLLER_COMMAND_ROW_ACCESS:
            case MEMORY_CONTROLLER_COMMAND_COLUMN_READ:
            case MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE:
	    
	    case MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS:
	    case MEMORY_CONTROLLER_COMMAND_COLBUFF_READ:
	    case MEMORY_CONTROLLER_COMMAND_COLBUFF_WRITE:
	    {
		bool open = false;
		if (this->bank_rowcol_state[bank] == true) {//check open COLUMN for special gathers
			if (package->memory_operation == MEMORY_OPERATION_SPECIAL_GATHER){
				open = (cmp_col_bank_channel(this->bank_open_row_address[bank], package->memory_address));
			} //else it remains false since other types of access cannot use the column buffer
		} else { //check open ROW!
			open = (cmp_row_bank_channel(this->bank_open_row_address[bank], package->memory_address));
		}

		
                //if (cmp_row_bank_channel(this->bank_open_row_address[bank], package->memory_address)) ///oh man, this can only go wrong. insertion here
              if (open){    
	      	switch (package->memory_operation) {
                        // HMC
                        case MEMORY_OPERATION_HMC_ROWA:
                            /// Respect Min. Latency
                            if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLUMN_READ) > sinuca_engine.get_global_cycle() ||
                            get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE) > sinuca_engine.get_global_cycle()) {
                                MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet. CAS->CAS (HMC INST.)\n");
                                break;
                            }
			    if (this->bank_rowcol_state[bank] == false){
                            	this->add_stat_row_buffer_hit();
			    } else {
			    	this->add_stat_col_buffer_hit();
			    }
                            this->bank_is_ready_to_transmit[bank] = true;
                        break;

                        // HMC
                        case MEMORY_OPERATION_HMC_ROA:
                        case MEMORY_OPERATION_READ:
                        case MEMORY_OPERATION_INST:
                        case MEMORY_OPERATION_PREFETCH:
                            /// Respect Min. Latency
                            if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLUMN_READ) > sinuca_engine.get_global_cycle()) {
                                MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet. CAS->CAS (READ)\n");
                                break;
                            }
			    if (this->bank_rowcol_state[bank] == false){
                            	this->add_stat_row_buffer_hit();
			    } else {
			    	this->add_stat_col_buffer_hit();
			    }
                            this->bank_is_ready_to_transmit[bank] = true;
                        break;

			//FBM SPECIAL GATHER
			case MEMORY_OPERATION_SPECIAL_GATHER:
                            /// Respect Min. Latency
                            if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLBUFF_READ) > sinuca_engine.get_global_cycle()) {
                                MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet. CAS->CAS (READ)\n");
                                break;
                            }
			    if (this->bank_rowcol_state[bank] == false){
                            	this->add_stat_row_buffer_hit();
			    } else {
			    	this->add_stat_col_buffer_hit();
				//printf("found open col\n");
			    }
                            this->bank_is_ready_to_transmit[bank] = true;
                        break;

                        case MEMORY_OPERATION_WRITEBACK:
                        case MEMORY_OPERATION_WRITE:
                            /// Respect Min. Latency
                            if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE) > sinuca_engine.get_global_cycle()) {
                                MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet. CAS->CAS (WRITE)\n");
                                break;
                            }
			    if (this->bank_rowcol_state[bank] == false){
                            	this->add_stat_row_buffer_hit();
			    } else {
			    	this->add_stat_col_buffer_hit();
			    }
                            this->bank_is_ready_to_transmit[bank] = true;
                        break;
                    }
                } else {
                    /// Respect Min. Latency

			if (package->memory_operation == MEMORY_OPERATION_SPECIAL_GATHER){
				/// Respect Min. Latency
				//printf("col was closed\n");
				if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS) > sinuca_engine.get_global_cycle()) {
				    MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet. RP->RAS for COLUMN (special gather)\n");
				    MEMORY_CONTROLLER_DEBUG_PRINTF("cycle %" PRIu64 ".\n", get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS));
				    break;
				} else {
				    MEMORY_CONTROLLER_DEBUG_PRINTF("COLBUFF_READ -> COLBUFF_ACCESS (different col).\n");
				    this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS;
				    this->bank_rowcol_state[bank] = true;//switch to open column
				    this->bank_open_row_address[bank] = package->memory_address;
				    this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] = sinuca_engine.get_global_cycle();
				    this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_ACCESS] = sinuca_engine.get_global_cycle();
				    this->add_stat_col_activates();
				}
			 } else {

			    if (get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_PRECHARGE) > sinuca_engine.get_global_cycle()) {
				MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency not ready yet.\n");
				break;
			    }

			    MEMORY_CONTROLLER_DEBUG_PRINTF("COLUMN_READ -> PRECHARGE (different row).\n");
			    this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_PRECHARGE;
			    this->bank_open_row_address[bank] = package->memory_address;
			    this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_PRECHARGE] = sinuca_engine.get_global_cycle();
			    this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_PRECHARGE] = sinuca_engine.get_global_cycle();
                	}
		}
	    }
	    break;

            // =====================================================================
            case MEMORY_CONTROLLER_COMMAND_NUMBER:
                ERROR_PRINTF("Should not receive COMMAND_NUMBER\n")
            break;
        }
    }


    /// Send in a round robin fashion the signals to COLUMN_READ / COLUMN_WRITE considering the tBurst latency
    uint32_t previous_bank_transmitted = this->last_bank_transmitted;

    for (uint32_t i = 0; i < this->get_bank_per_channel(); i++) {
        this->last_bank_transmitted++;
        if (this->last_bank_transmitted >= this->get_bank_per_channel()) {
            this->last_bank_transmitted = 0;
        }
        if (this->bank_is_ready_to_transmit[last_bank_transmitted]) {
            break;
        }
    }
    bank = last_bank_transmitted;

    if (this->bank_is_ready_to_transmit[bank]) {
        ERROR_ASSERT_PRINTF(this->bank_buffer_actual_position[bank] != POSITION_FAIL, "Sending READ/WRITE from no pre-selected bank")
        /// Respect Min. tBurst Latency
        if (this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_READ] > sinuca_engine.get_global_cycle() ||
        this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] > sinuca_engine.get_global_cycle() || 
	this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_READ] > sinuca_engine.get_global_cycle() ||
	this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_WRITE] > sinuca_engine.get_global_cycle())
	{
            MEMORY_CONTROLLER_DEBUG_PRINTF("Minimum latency (tBurst) not ready yet. \n");
            this->last_bank_transmitted = previous_bank_transmitted;
            return;
        }

        memory_package_t *package = this->bank_buffer[bank][this->bank_buffer_actual_position[bank]];
        MEMORY_CONTROLLER_DEBUG_PRINTF("Channel Treating %s\n", package->content_to_string().c_str());

        // =====================================================================
	if (this->bank_rowcol_state[bank] == false)
	{
		ERROR_ASSERT_PRINTF(cmp_row_bank_channel(this->bank_open_row_address[bank], package->memory_address), "Sending READ/WRITE to wrong row. state %d, this->bank_open_row_address %lu, package address %lu\n", this->bank_rowcol_state[bank],this->bank_open_row_address[bank], package->memory_address)
	} else {
		ERROR_ASSERT_PRINTF(cmp_col_bank_channel(this->bank_open_row_address[bank], package->memory_address), "Sending READ/WRITE to wrong col.\n")
	}
        switch (package->memory_operation) {
            // HMC
            case MEMORY_OPERATION_HMC_ROWA:   /// Perform: READ + OPERATION + UPDATE (WRITE)
            {
                MEMORY_CONTROLLER_DEBUG_PRINTF("ROW_ACCESS -> COLUMN_READ.\n");
                this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE;
                this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLUMN_READ] = sinuca_engine.get_global_cycle() + this->hmc_timing_burst;
                this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_READ] = sinuca_engine.get_global_cycle() + this->hmc_timing_burst;

                uint64_t r_to_w_latency = this->timing_cas + this->hmc_timing_burst + this->hmc_latency_rowa;

                this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] = sinuca_engine.get_global_cycle() + r_to_w_latency + this->hmc_timing_burst;
                this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] = sinuca_engine.get_global_cycle() + r_to_w_latency + this->hmc_timing_burst;

                /// Prepare for answer later
                package->is_answer = true;
                package->package_transmit(r_to_w_latency + this->timing_cwd + this->hmc_timing_burst);

            }
            break;

            // HMC
            case MEMORY_OPERATION_HMC_ROA: /// Perform: READ + OPERATION
            {
                MEMORY_CONTROLLER_DEBUG_PRINTF("ROW_ACCESS -> COLUMN_READ.\n");
                this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_COLUMN_READ;
                this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLUMN_READ] = sinuca_engine.get_global_cycle() + this->hmc_timing_burst;
                this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_READ] = sinuca_engine.get_global_cycle() + this->hmc_timing_burst;
                /// Prepare for answer later
                package->memory_size = sinuca_engine.get_global_line_size();
                package->is_answer = true;
                package->package_transmit(this->timing_cas + this->hmc_timing_burst);
            }
            break;

            case MEMORY_OPERATION_READ:
            case MEMORY_OPERATION_INST:
            case MEMORY_OPERATION_PREFETCH:
            {    
	    	MEMORY_CONTROLLER_DEBUG_PRINTF("ROW_ACCESS -> COLUMN_READ.\n");
                this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_COLUMN_READ;
                this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLUMN_READ] = sinuca_engine.get_global_cycle() + this->timing_burst;
                this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_READ] = sinuca_engine.get_global_cycle() + this->timing_burst;
                /// Prepare for answer later
		package->memory_size = sinuca_engine.get_global_line_size();
                package->is_answer = true;
                package->package_transmit(this->timing_cas + this->timing_burst);
	    }
            break;
	    case MEMORY_OPERATION_SPECIAL_GATHER:
	    {
                MEMORY_CONTROLLER_DEBUG_PRINTF("COLBUFF_ACCESS -> COLBUFF_READ.\n");
                this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_COLBUFF_READ;
                this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLBUFF_READ] = sinuca_engine.get_global_cycle() + this->timing_burst;
                this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLBUFF_READ] = sinuca_engine.get_global_cycle() + this->timing_burst;
                /// Prepare for answer later
                package->memory_size = sinuca_engine.get_global_line_size();
                package->is_answer = true;
                package->package_transmit(this->timing_cas + this->timing_burst);
	    }
	    break;
            case MEMORY_OPERATION_WRITEBACK:
            case MEMORY_OPERATION_WRITE:
	    {
                MEMORY_CONTROLLER_DEBUG_PRINTF("ROW_ACCESS -> COLUMN_WRITE.\n");
                this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE;
                this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] = sinuca_engine.get_global_cycle() + this->timing_burst;
                this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_COLUMN_WRITE] = sinuca_engine.get_global_cycle() + this->timing_burst;
                /// Prepare for answer later
                package->memory_size = 1;
                package->is_answer = true;
                package->package_ready(this->timing_cwd + this->timing_burst);
	    }
	    break;

        }
        this->bank_buffer[bank].erase(this->bank_buffer[bank].begin() + this->bank_buffer_actual_position[bank]);
        this->packages_inside_channel--;
        this->bank_buffer_actual_position[bank] = POSITION_FAIL;
        this->bank_is_ready_to_transmit[bank] = false;

        //==================================================================
        // CONTROL THE PAGE_POLICY
        // If page_policy == close_row
        if (this->page_policy == PAGE_POLICY_CLOSE_ROW) {
            /// Select package to be treated
            this->bank_buffer_actual_position[bank] = find_next_package(bank);
            // If buffer is empty --->>> RowPrecharge
            if (this->bank_buffer_actual_position[bank] == POSITION_FAIL) {
                uint64_t latency_ready_cycle = get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_ROW_ACCESS);
                // Row precharge code here!!!!
                MEMORY_CONTROLLER_DEBUG_PRINTF("COLUMN_READ -> PRECHARGE (empty buffer).\n");
                MEMORY_CONTROLLER_DEBUG_PRINTF("PRECHARGE will happen after RAS on cycle %" PRIu64 ".\n", latency_ready_cycle);
                this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_ROW_ACCESS;
                this->bank_open_row_address[bank] = 0;
		this->bank_rowcol_state[bank] = false;
                this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_PRECHARGE] = latency_ready_cycle;
                this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_PRECHARGE] = latency_ready_cycle;
            }
            else {
                package = this->bank_buffer[bank][this->bank_buffer_actual_position[bank]];
                // If next package == row buffer miss --->>> RowPrecharge
                if (!cmp_row_bank_channel(this->bank_open_row_address[bank], package->memory_address)) {
                    uint64_t latency_ready_cycle = get_minimum_latency(bank, MEMORY_CONTROLLER_COMMAND_ROW_ACCESS);
                    // Row precharge code here!!!!
                    MEMORY_CONTROLLER_DEBUG_PRINTF("COLUMN_READ -> PRECHARGE (different row).\n");
                    MEMORY_CONTROLLER_DEBUG_PRINTF("PRECHARGE will happen after RAS on cycle %" PRIu64 ".\n", latency_ready_cycle);
                    this->bank_last_command[bank] = MEMORY_CONTROLLER_COMMAND_PRECHARGE;
                    this->bank_open_row_address[bank] = package->memory_address;
                    this->bank_last_command_cycle[bank][MEMORY_CONTROLLER_COMMAND_PRECHARGE] = latency_ready_cycle;
                    this->channel_last_command_cycle[MEMORY_CONTROLLER_COMMAND_PRECHARGE] = latency_ready_cycle;
                }
            }
        }
    }
};


// ============================================================================
/// Bank Selection Strategies
// ============================================================================

// ============================================================================
/// Selection strategy: Round Robin
uint32_t memory_channel_t::selection_bank_round_robin() {

    for (uint32_t i = 0; i < this->get_bank_per_channel(); i++) {
        this->last_bank_selected++;
        if (this->last_bank_selected >= this->get_bank_per_channel()) {
            this->last_bank_selected = 0;
        }
        if (!this->bank_buffer[last_bank_selected].empty()) {
            break;
        }
    }
    return this->last_bank_selected;
};

// ============================================================================
/// Selection strategy: Random
uint32_t memory_channel_t::selection_bank_random() {
    unsigned int seed = sinuca_engine.get_global_cycle() % 1000;
    uint32_t selected = (rand_r(&seed) % this->get_bank_per_channel());
    return selected;
};


// ============================================================================
int32_t memory_channel_t::send_package(memory_package_t *package) {
    ERROR_PRINTF("Send package %s.\n", package->content_to_string().c_str());
    return POSITION_FAIL;
};

// ============================================================================
bool memory_channel_t::receive_package(memory_package_t *package, uint32_t input_port, uint32_t transmission_latency) {
    ERROR_PRINTF("Received package %s into the input_port %u, latency %u.\n", package->content_to_string().c_str(), input_port, transmission_latency);
    return FAIL;
};

// ============================================================================
/// Token Controller Methods
// ============================================================================
bool memory_channel_t::pop_token_credit(uint32_t src_id, memory_operation_t memory_operation) {
    ERROR_PRINTF("pop_token_credit %" PRIu32 " %s.\n", src_id, get_enum_memory_operation_char(memory_operation))
    return FAIL;
};

// ============================================================================
void memory_channel_t::print_structures() {
    for (uint32_t i = 0; i < this->get_bank_per_channel(); i++) {
        SINUCA_PRINTF("%s BANK_BUFFER[%s]\n", this->get_label(), utils_t::uint32_to_string(i).c_str());
        for (uint32_t j = 0; j < this->bank_buffer[i].size(); j++) {
            SINUCA_PRINTF("%s BANK_BUFFER[%s][%s] %s\n", this->get_label(),
                                                        utils_t::uint32_to_string(i).c_str(),
                                                        utils_t::uint32_to_string(j).c_str(),
                                                        this->bank_buffer[i][j]->content_to_string().c_str());
        }
    }
};

// ============================================================================
void memory_channel_t::panic() {
    this->print_structures();
};

// ============================================================================
void memory_channel_t::periodic_check(){
    #ifdef MEMORY_CONTROLLER_DEBUG
        this->print_structures();
    #endif
};

// ============================================================================
/// STATISTICS
// ============================================================================
void memory_channel_t::reset_statistics() {
    this->stat_row_buffer_hit = 0;
    this->stat_row_buffer_miss = 0;
    this->stat_col_buffer_hit = 0;
    this->stat_col_buffer_miss = 0;

    this->stat_read_forward = 0;
    this->stat_write_forward = 0;
};

// ============================================================================
void memory_channel_t::print_statistics() {
     char title[1024] = "";
    snprintf(title, sizeof(title), "Configuration of %s", this->get_label());
    sinuca_engine.write_statistics_big_separator();
    sinuca_engine.write_statistics_comments(title);
    sinuca_engine.write_statistics_big_separator();

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_read_forward", stat_read_forward);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_write_forward", stat_write_forward);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_row_buffer_hit", stat_row_buffer_hit );
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_row_buffer_miss", stat_row_buffer_miss);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_col_buffer_hit", stat_col_buffer_hit );
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_col_buffer_miss", stat_col_buffer_miss);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_col_activates", stat_col_activates);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "stat_row_activates", stat_row_activates);
};

// ============================================================================
void memory_channel_t::print_configuration() {
    char title[1024] = "";
    snprintf(title, sizeof(title), "Configuration of %s", this->get_label());
    sinuca_engine.write_statistics_big_separator();
    sinuca_engine.write_statistics_comments(title);
    sinuca_engine.write_statistics_big_separator();
    
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "bank_per_channel", bank_per_channel);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "bank_buffer_size", bank_buffer_size);

    sinuca_engine.write_statistics_small_separator();
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_burst", timing_burst);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_al", timing_al);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_cas", timing_cas);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_ccd", timing_ccd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_cwd", timing_cwd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_faw", timing_faw);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_ras", timing_ras);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rc", timing_rc);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rcd", timing_rcd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rp", timing_rp);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rrd", timing_rrd);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_rtp", timing_rtp);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_wr", timing_wr);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "timing_wtr", timing_wtr);

    // HMC
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hmc_latency_roa", hmc_latency_roa);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "hmc_latency_rowa", hmc_latency_rowa);
    sinuca_engine.write_statistics_value(get_type_component_label(), get_label(), "colbuff_strobe_count", colbuff_strobe_count);


};



