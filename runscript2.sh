#!/bin/bash

for f in /home/fbm/workspec/teste*.tid0.stat.out.gz; do
	inp=$(echo "${f%.tid0.stat.out.gz}")
	out=$(echo "${inp}.AVcoproc.output")
	echo $inp
	echo $out
	./sinuca -config config_examples/sandy_8gambicores/sandy_8cores.cfg -trace $inp >> $out &
done;
wait
