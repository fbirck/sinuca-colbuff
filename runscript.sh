#!/bin/bash

for f in /home/fbm/workspec/7idx*.tid0.stat.out.gz; do
	inp=$(echo "${f%.tid0.stat.out.gz}")
	out=$(echo "${inp}.ONLY_libc7calls.output")
	echo $inp
	echo $out
	./sinuca -config config_examples/sandy_8gambicores/sandy_8cores.cfg -trace $inp > $out &
done;
wait
